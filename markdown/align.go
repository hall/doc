package markdown

type Align byte

const (
	AlignNone = iota
	AlignLeft
	AlignCenter
	AlignRight
)

func (a Align) String() string {
	switch a {
	case AlignLeft:
		return "left"
	case AlignCenter:
		return "center"
	case AlignRight:
		return "right"
	}
	return ""
}
