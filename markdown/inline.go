package markdown

func ruleInline(s *StateCore) {
	for _, tok := range s.Tokens {
		if tok, ok := tok.(*Inline); ok {
			tok.Children = s.Md.Inline.Parse(tok.Content, s.Md, s.Env)
		}
	}
}
