package markdown

type StateCore struct {
	Src    string
	TokArr [3]Token
	Tokens []Token
	Md     *Markdown
	Env    *Environment
}
